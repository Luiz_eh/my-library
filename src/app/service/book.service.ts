import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Book } from "../model/book.model";
import { DefaultResponse } from "../model/response.model";

const URL_BASE = "http://localhost:8080/book"
const URL_FIND_BY_TITLE = URL_BASE + "/byTitle?title="
const URL_FIND_BY_ISBN = URL_BASE + "/byIsbn/"


@Injectable({
    providedIn: 'root'
})
export class BookService {

    constructor(private http: HttpClient) {

    }

    findAll(): Observable< DefaultResponse<Book[]> > {
        return this.http.get< DefaultResponse<Book[]> >(URL_BASE)
    }

    findByTitle(title: string): Observable< DefaultResponse<Book[]>> {
        return this.http.get< DefaultResponse<Book[]> >(URL_FIND_BY_TITLE + title)
    }
}