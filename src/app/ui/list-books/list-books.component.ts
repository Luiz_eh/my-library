import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Book } from 'src/app/model/book.model';
import { DefaultResponse } from 'src/app/model/response.model';
import { BookService } from 'src/app/service/book.service';

@Component({
    selector: 'app-list-books',
    templateUrl: './list-books.component.html',
    styleUrls: ['./list-books.component.css'],
})
export class ListBooksComponent implements OnInit {

    books: Book[] = []

    formSearch = this.formBuilder.group({
        searchType: [1, [Validators.required]],
        searchText: ['', [Validators.required]]
    })

    constructor(private formBuilder: FormBuilder, 
                private bookService: BookService) { }

    ngOnInit() {
        this.findBooks()
    }

    findBooks() {
        this.bookService.findAll().subscribe(
            response => this.onSearchResult(response)
        )
    }

    search() {
        if (this.searchType.value == 1) {
            this.bookService.findByTitle(this.searchText.value).subscribe(
                response => this.onSearchResult(response)
                )
        }
    }

    onSearchResult(response: DefaultResponse<Book[]>) {
        if (response != null) {
            this.books = response.result
        }
    }

    clearSearch() {
        this.searchText.setValue("")
        this.searchType.setValue(1)
    }

    get searchType() {
        return this.formSearch.controls.searchType
    }

    get searchText() {
        return this.formSearch.controls.searchText
    }
}
